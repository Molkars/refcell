#![allow(unused)]

extern crate core;

use std;
use std::cell::{Cell, UnsafeCell};
use std::cmp::Ordering;
use std::fmt::{Debug, Display, Formatter};
use std::ops::{Deref, DerefMut};

type BorrowFlag = isize;

const UNUSED: BorrowFlag = 0;

#[inline(always)]
fn is_writing(x: BorrowFlag) -> bool { x < UNUSED }

fn is_reading(x: BorrowFlag) -> bool { x > UNUSED }

pub struct RefCell<T: ?Sized> {
    borrow: Cell<BorrowFlag>,
    value: UnsafeCell<T>,
}

impl<T: ?Sized + Debug> Debug for RefCell<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("RefCell")
            .field("value", unsafe { &&*self.value.get() })
            .finish()
    }
}

#[non_exhaustive]
pub struct BorrowError;

impl Debug for BorrowError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("BorrowError").finish()
    }
}

impl Display for BorrowError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Display::fmt("already mutably borrowed", f)
    }
}

#[non_exhaustive]
pub struct BorrowMutError;

impl Debug for BorrowMutError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("BorrowMutErr").finish()
    }
}


impl Display for BorrowMutError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Display::fmt("already borrowed", f)
    }
}

impl<T> RefCell<T> {
    pub const fn new(value: T) -> Self {
        Self {
            value: UnsafeCell::new(value),
            borrow: Cell::new(UNUSED),
        }
    }

    #[inline]
    pub fn into_inner(self) -> T {
        self.value.into_inner()
    }

    #[inline]
    pub fn replace(&self, t: T) -> T {
        std::mem::replace(&mut *self.borrow_mut(), t)
    }

    #[inline]
    pub fn replace_with<F: FnOnce(&mut T) -> T>(&self, f: F) -> T {
        let mut_borrow = &mut *self.borrow_mut();
        let replacement = f(mut_borrow);
        std::mem::replace(mut_borrow, replacement)
    }

    #[inline]
    pub fn swap(&self, other: &Self) {
        std::mem::swap(&mut *self.borrow_mut(), &mut *other.borrow_mut())
    }
}

impl<T: ?Sized> RefCell<T> {
    #[inline]
    pub fn borrow(&self) -> Ref<'_, T> {
        self.try_borrow().expect("already mutably borrowed")
    }

    #[inline]
    pub fn try_borrow(&self) -> Result<Ref<'_, T>, BorrowError> {
        match BorrowRef::new(&self.borrow) {
            Some(b) => Ok(Ref { value: unsafe { &*self.value.get() }, borrow: b }),
            None => Err(BorrowError)
        }
    }

    #[inline]
    pub fn borrow_mut(&self) -> RefMut<'_, T> {
        self.try_borrow_mut().expect("already borrowed")
    }

    #[inline]
    pub fn try_borrow_mut(&self) -> Result<RefMut<'_, T>, BorrowMutError> {
        match BorrowRefMut::new(&self.borrow) {
            Some(b) => Ok(RefMut {
                value: unsafe { &mut *self.value.get() },
                borrow: b,
            }),
            None => Err(BorrowMutError),
        }
    }

    #[inline]
    pub fn as_ptr(&self) -> *mut T {
        self.value.get()
    }

    #[inline]
    pub fn get_mut(&mut self) -> &mut T {
        self.value.get_mut()
    }
}

impl<T: Default> RefCell<T> {
    pub fn take(&self) -> T {
        self.replace(Default::default())
    }
}

unsafe impl<T: ?Sized> Send for RefCell<T> where T: Send {}

impl<T: Clone> Clone for RefCell<T> {
    #[inline]
    fn clone(&self) -> Self {
        RefCell::new(self.borrow().clone())
    }

    fn clone_from(&mut self, source: &Self) {
        self.get_mut().clone_from(&source.borrow())
    }
}

impl<T: Default> Default for RefCell<T> {
    #[inline]
    fn default() -> Self {
        RefCell::new(Default::default())
    }
}

impl<T: ?Sized + PartialEq> PartialEq for RefCell<T> {
    fn eq(&self, other: &Self) -> bool {
        *self.borrow() == *other.borrow()
    }
}

impl<T: ?Sized + PartialEq + Eq> Eq for RefCell<T> {}

impl<T: ?Sized + PartialOrd> PartialOrd for RefCell<T> {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.borrow().partial_cmp(&other.borrow())
    }

    #[inline]
    fn lt(&self, other: &Self) -> bool {
        *self.borrow() < *other.borrow()
    }

    #[inline]
    fn le(&self, other: &Self) -> bool {
        *self.borrow() <= *other.borrow()
    }

    #[inline]
    fn gt(&self, other: &Self) -> bool {
        *self.borrow() > *other.borrow()
    }

    #[inline]
    fn ge(&self, other: &Self) -> bool {
        *self.borrow() >= *other.borrow()
    }
}

impl<T: ?Sized + PartialOrd + Ord> Ord for RefCell<T> {
    #[inline]
    fn cmp(&self, other: &Self) -> Ordering {
        self.borrow().cmp(&other.borrow())
    }
}

impl<T> From<T> for RefCell<T> {
    fn from(t: T) -> Self {
        RefCell::new(t)
    }
}

struct BorrowRef<'b> {
    borrow: &'b Cell<BorrowFlag>,
}

impl<'b> BorrowRef<'b> {
    #[inline]
    fn new(borrow: &'b Cell<BorrowFlag>) -> Option<Self> {
        let b = borrow.get().wrapping_add(1);
        if !is_reading(b) {
            None
        } else {
            borrow.set(b);
            Some(BorrowRef { borrow })
        }
    }
}

impl Drop for BorrowRef<'_> {
    #[inline]
    fn drop(&mut self) {
        let borrow = self.borrow.get();
        debug_assert!(is_reading(borrow));
        self.borrow.set(borrow - 1);
    }
}

impl Clone for BorrowRef<'_> {
    #[inline]
    fn clone(&self) -> Self {
        let borrow = self.borrow.get();
        debug_assert!(is_reading(borrow));
        assert_ne!(borrow, isize::MAX);
        self.borrow.set(borrow + 1);
        BorrowRef { borrow: self.borrow }
    }
}

pub struct Ref<'a, T: ?Sized + 'a> {
    value: &'a T,
    borrow: BorrowRef<'a>,
}

impl<T: ?Sized + Debug> Debug for Ref<'_, T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Ref")
            .field("value", &self.value)
            .finish()
    }
}

impl<T: ?Sized + PartialEq> PartialEq for Ref<'_, T> {
    fn eq(&self, other: &Self) -> bool {
        self.value.eq(other.value)
    }
}

impl<T: ?Sized + PartialEq + Eq> Eq for Ref<'_, T> {}

impl<T: ?Sized + PartialOrd> PartialOrd for Ref<'_, T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.value.partial_cmp(other.value)
    }

    fn lt(&self, other: &Self) -> bool {
        self.value < other.value
    }

    fn le(&self, other: &Self) -> bool {
        self.value <= other.value
    }

    fn gt(&self, other: &Self) -> bool {
        self.value > other.value
    }

    fn ge(&self, other: &Self) -> bool {
        self.value >= other.value
    }
}

impl<T: ?Sized + PartialEq + Eq + PartialOrd + Ord> Ord for Ref<'_, T> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.value.cmp(other.value)
    }
}

impl<T: ?Sized> Deref for Ref<'_, T> {
    type Target = T;

    #[inline]
    fn deref(&self) -> &Self::Target {
        self.value
    }
}

impl<'a, T: ?Sized> Ref<'a, T> {
    #[must_use]
    #[inline]
    pub fn clone(orig: &Ref<'a, T>) -> Ref<'a, T> {
        Ref {
            value: orig.value,
            borrow: orig.borrow.clone(),
        }
    }

    pub fn map<U: ?Sized, F>(&self, f: F) -> Ref<'a, U>
        where
            F: FnOnce(&T) -> &U, {
        Ref {
            value: f(self.value),
            borrow: self.borrow.clone(),
        }
    }

    pub fn map_opt<U: ?Sized, F>(&self, f: F) -> Option<Ref<'a, U>>
        where
            F: FnOnce(&T) -> Option<&U>,
    {
        match f(self.value) {
            None => None,
            Some(value) => Some(Ref {
                value,
                borrow: self.borrow.clone(),
            })
        }
    }

    pub fn filter_map<U: ?Sized, E, F>(&self, f: F) -> Result<Ref<'a, U>, E>
        where
            F: FnOnce(&T) -> Result<&U, E>,
    {
        match f(self.value) {
            Ok(value) => Ok(Ref {
                value,
                borrow: self.borrow.clone(),
            }),
            Err(e) => Err(e)
        }
    }
}

struct BorrowRefMut<'b> {
    borrow: &'b Cell<BorrowFlag>,
}

impl Drop for BorrowRefMut<'_> {
    #[inline]
    fn drop(&mut self) {
        let borrow = self.borrow.get();
        debug_assert!(is_writing(borrow));
        self.borrow.set(borrow + 1);
    }
}

impl<'b> BorrowRefMut<'b> {
    #[inline]
    fn new(borrow: &'b Cell<BorrowFlag>) -> Option<BorrowRefMut<'b>> {
        match borrow.get() {
            UNUSED => {
                borrow.set(UNUSED - 1);
                Some(BorrowRefMut { borrow })
            }
            _ => None,
        }
    }

    #[inline]
    fn clone(&self) -> BorrowRefMut<'b> {
        let borrow = self.borrow.get();
        debug_assert!(is_writing(borrow));
        assert_ne!(borrow, isize::MIN);
        self.borrow.set(borrow - 1);
        BorrowRefMut { borrow: self.borrow }
    }
}

pub struct RefMut<'b, T: ?Sized + 'b> {
    value: &'b mut T,
    borrow: BorrowRefMut<'b>,
}

impl<T: ?Sized> Deref for RefMut<'_, T> {
    type Target = T;

    #[inline]
    fn deref(&self) -> &T {
        self.value
    }
}

impl<T: ?Sized> DerefMut for RefMut<'_, T> {
    #[inline]
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.value
    }
}

impl<T: ?Sized + Display> Display for RefMut<'_, T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.value.fmt(f)
    }
}

impl<'b, T: ?Sized + 'b> RefMut<'b, T> {
    pub fn map<U: ?Sized, F>(self, f: F) -> RefMut<'b, U>
        where
            F: FnOnce(&mut T) -> &mut U,
    {
        RefMut {
            value: f(self.value),
            borrow: self.borrow,
        }
    }
}

#[cfg(test)]
mod tests {
    use std::collections::BTreeMap;
    use std::ops::Deref;

    use crate::RefCell;

    #[test]
    #[should_panic(expected = "already mutably borrowed: BorrowError")]
    fn double_borrow() {
        let val = RefCell::new(0);
        let mut_borrow = val.borrow_mut();
        let borrow = val.borrow();
    }

    #[test]
    #[should_panic(expected = "already borrowed: BorrowMutErr")]
    fn double_mutable_borrow() {
        let val = RefCell::new(0);
        let a = val.borrow_mut();
        let b = val.borrow_mut();
    }

    #[test]
    fn unwrap_opt() {
        let val = RefCell::new(BTreeMap::from([(0, 0)]));
        let a = val.borrow().map_opt(|tree| tree.get(&0));
        let b = val.borrow().map_opt(|tree| tree.get(&1));
        match a {
            Some(val) => assert_eq!(val.deref(), &0i32),
            None => panic!("no value"),
        }
        assert_eq!(b, None);
    }

    #[test]
    fn map_opt() {
        let val = RefCell::new(vec![1, 2, 3]);
        let borrow = val.borrow();
        let fourth = borrow.map_opt(|v| v.get(3));
        let second = borrow.map_opt(|v| v.get(1));
        assert_eq!(fourth, None);
        assert_eq!(second.map(|v| v.deref().clone()), Some(2));
    }
}
