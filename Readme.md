# RefCell

*A re-implementation of the rust std::cell::RefCell*

Features:

* `RefCell`: A re-implementation of the rust std::cell::RefCell
* `Ref`: A re-implementation of the rust std::cell::Ref
  * Allow moving options and results out of `Ref<'_, Option<T>>`
  ```rust
  use refcell::{RefCell, Ref};
  // ...
  let cell = RefCell::new(BTreeMap::from([(0, 0)]));
  let val: Option<Ref<i32>> = cell.borrow().map_opt(|v| v.get(0));
  ```